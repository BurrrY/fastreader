﻿namespace FastReader
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tb_Word = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.nud_WPM = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Input = new System.Windows.Forms.Button();
            this.cb_ReplaceWikiReference = new System.Windows.Forms.CheckBox();
            this.nudCommaPause = new System.Windows.Forms.NumericUpDown();
            this.nudDotPause = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.cb_onTop = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.nud_Font = new System.Windows.Forms.NumericUpDown();
            this.lbl_BuildDate = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nud_WPM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCommaPause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDotPause)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_Font)).BeginInit();
            this.SuspendLayout();
            // 
            // tb_Word
            // 
            this.tb_Word.BackColor = System.Drawing.Color.White;
            this.tb_Word.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Word.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_Word.Enabled = false;
            this.tb_Word.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Word.ForeColor = System.Drawing.Color.Black;
            this.tb_Word.Location = new System.Drawing.Point(0, 0);
            this.tb_Word.Name = "tb_Word";
            this.tb_Word.ReadOnly = true;
            this.tb_Word.Size = new System.Drawing.Size(283, 28);
            this.tb_Word.TabIndex = 0;
            this.tb_Word.Text = "FastReader";
            this.tb_Word.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_Word.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_Word_KeyDown);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(118, 104);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(34, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // nud_WPM
            // 
            this.nud_WPM.Location = new System.Drawing.Point(52, 117);
            this.nud_WPM.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nud_WPM.Name = "nud_WPM";
            this.nud_WPM.Size = new System.Drawing.Size(60, 20);
            this.nud_WPM.TabIndex = 2;
            this.nud_WPM.Value = new decimal(new int[] {
            400,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "WPM:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Words:";
            // 
            // btn_Input
            // 
            this.btn_Input.Location = new System.Drawing.Point(118, 127);
            this.btn_Input.Name = "btn_Input";
            this.btn_Input.Size = new System.Drawing.Size(34, 23);
            this.btn_Input.TabIndex = 8;
            this.btn_Input.Text = "ins";
            this.btn_Input.UseVisualStyleBackColor = true;
            this.btn_Input.Click += new System.EventHandler(this.btn_Input_Click);
            // 
            // cb_ReplaceWikiReference
            // 
            this.cb_ReplaceWikiReference.AutoSize = true;
            this.cb_ReplaceWikiReference.Checked = true;
            this.cb_ReplaceWikiReference.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_ReplaceWikiReference.Location = new System.Drawing.Point(14, 184);
            this.cb_ReplaceWikiReference.Name = "cb_ReplaceWikiReference";
            this.cb_ReplaceWikiReference.Size = new System.Drawing.Size(148, 17);
            this.cb_ReplaceWikiReference.TabIndex = 9;
            this.cb_ReplaceWikiReference.Text = "Replace Wiki-References";
            this.cb_ReplaceWikiReference.UseVisualStyleBackColor = true;
            // 
            // nudCommaPause
            // 
            this.nudCommaPause.Location = new System.Drawing.Point(14, 207);
            this.nudCommaPause.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudCommaPause.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCommaPause.Name = "nudCommaPause";
            this.nudCommaPause.Size = new System.Drawing.Size(61, 20);
            this.nudCommaPause.TabIndex = 10;
            this.nudCommaPause.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // nudDotPause
            // 
            this.nudDotPause.Location = new System.Drawing.Point(14, 233);
            this.nudDotPause.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudDotPause.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDotPause.Name = "nudDotPause";
            this.nudDotPause.Size = new System.Drawing.Size(61, 20);
            this.nudDotPause.TabIndex = 11;
            this.nudDotPause.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(81, 209);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Extra-Pause after Comma";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(82, 235);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Extra-Pause after Dots";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(14, 74);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(257, 13);
            this.progressBar1.TabIndex = 14;
            // 
            // cb_onTop
            // 
            this.cb_onTop.AutoSize = true;
            this.cb_onTop.Checked = true;
            this.cb_onTop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_onTop.Location = new System.Drawing.Point(14, 259);
            this.cb_onTop.Name = "cb_onTop";
            this.cb_onTop.Size = new System.Drawing.Size(84, 17);
            this.cb_onTop.TabIndex = 15;
            this.cb_onTop.Text = "Stay on Top";
            this.cb_onTop.UseVisualStyleBackColor = true;
            this.cb_onTop.CheckedChanged += new System.EventHandler(this.cb_onTop_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.tb_Word);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(283, 59);
            this.panel1.TabIndex = 16;
            // 
            // btnBack
            // 
            this.btnBack.BackgroundImage = global::FastReader.Properties.Resources._return;
            this.btnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnBack.Enabled = false;
            this.btnBack.Location = new System.Drawing.Point(236, 112);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(33, 33);
            this.btnBack.TabIndex = 6;
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnPause
            // 
            this.btnPause.BackColor = System.Drawing.SystemColors.Control;
            this.btnPause.BackgroundImage = global::FastReader.Properties.Resources.pause;
            this.btnPause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPause.Enabled = false;
            this.btnPause.Location = new System.Drawing.Point(197, 113);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(33, 33);
            this.btnPause.TabIndex = 5;
            this.btnPause.UseVisualStyleBackColor = false;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackgroundImage = global::FastReader.Properties.Resources.start;
            this.btnStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnStart.Enabled = false;
            this.btnStart.Location = new System.Drawing.Point(158, 112);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(33, 33);
            this.btnStart.TabIndex = 4;
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(82, 286);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Font-Size";
            // 
            // nud_Font
            // 
            this.nud_Font.Location = new System.Drawing.Point(14, 282);
            this.nud_Font.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nud_Font.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nud_Font.Name = "nud_Font";
            this.nud_Font.Size = new System.Drawing.Size(61, 20);
            this.nud_Font.TabIndex = 17;
            this.nud_Font.Value = new decimal(new int[] {
            18,
            0,
            0,
            0});
            this.nud_Font.ValueChanged += new System.EventHandler(this.nud_Font_ValueChanged);
            // 
            // lbl_BuildDate
            // 
            this.lbl_BuildDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_BuildDate.AutoSize = true;
            this.lbl_BuildDate.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.lbl_BuildDate.Location = new System.Drawing.Point(174, 294);
            this.lbl_BuildDate.Name = "lbl_BuildDate";
            this.lbl_BuildDate.Size = new System.Drawing.Size(105, 13);
            this.lbl_BuildDate.TabIndex = 20;
            this.lbl_BuildDate.Text = "Build on: 2000-00-00";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(283, 311);
            this.Controls.Add(this.lbl_BuildDate);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nud_Font);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cb_onTop);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nudDotPause);
            this.Controls.Add(this.nudCommaPause);
            this.Controls.Add(this.cb_ReplaceWikiReference);
            this.Controls.Add(this.btn_Input);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nud_WPM);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "FastReader";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_Word_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.nud_WPM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCommaPause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDotPause)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_Font)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_Word;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown nud_WPM;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Input;
        private System.Windows.Forms.CheckBox cb_ReplaceWikiReference;
        private System.Windows.Forms.NumericUpDown nudCommaPause;
        private System.Windows.Forms.NumericUpDown nudDotPause;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.CheckBox cb_onTop;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nud_Font;
        private System.Windows.Forms.Label lbl_BuildDate;
    }
}

