﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace FastReader
{
    public partial class Form1 : Form
    {
        private string text;
        private List<string> splitText = new List<string>();
        private Thread readerThread;
        private int currentPos = 0;
        private bool threadRunning = false;
        private int wordCount;
        private config c = new config();

        public Form1()
        {
            InitializeComponent();
            this.Text = "FastReader v" + info.buildNo;
            lbl_BuildDate.Text = "Build on " + info.buildDate.Substring(0, 10);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string file = ofd.FileName;
                StreamReader sr = new StreamReader(file);
                text = sr.ReadToEnd();
                splitText = text.Split(' ').ToList<string>();
                wordCount = splitText.Count;
                label2.Text = "Words:" + wordCount.ToString();
                btnBack.Enabled = btnPause.Enabled = btnStart.Enabled = true;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            start();
        }

        private void start()
        {

            threadRunning = true;
            ThreadStart ts = new ThreadStart(readText);
            readerThread = new Thread(ts);
            readerThread.Start();
        }

        private void readText()
        {
            while (currentPos < splitText.Count && threadRunning)
            {
                string text = splitText[currentPos++];
                try
                {
                    SetText(text);
                    remainingWords("Words:" + (wordCount-currentPos).ToString() + "/" + wordCount.ToString());
                    setBar(currentPos);
                }
                catch (Exception e) { }

                if (text.EndsWith("."))
                    Thread.Sleep((int)nudDotPause.Value);

                if (text.EndsWith(","))
                    Thread.Sleep((int)nudCommaPause.Value);

                if (text.Length > 12)
                    Thread.Sleep(10);

                Thread.Sleep((1000*60) / (int)nud_WPM.Value);
            }
        }



        private void remainingWords(string text)
        {
            if (label2.InvokeRequired)
                label2.Invoke(new Action<string>(this.remainingWords), new object[] { text });
            else
                label2.Text = text;            
        }

        private void SetText(string text)
        {
            if (tb_Word.InvokeRequired)
                tb_Word.Invoke(new Action<string>(this.SetText), new object[] { text });
            else
                tb_Word.Text = text;            
        }

        private void setBar(int val)
        {
            if (progressBar1.InvokeRequired)
                progressBar1.Invoke(new Action<int>(this.setBar), new object[] { val });
            else
                progressBar1.Value = val;
        }


        private void btnPause_Click(object sender, EventArgs e)
        {
            pause();
        }

        private void pause()
        {
            threadRunning = false;

            if(readerThread != null)
                readerThread.Abort();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            currentPos = 0;
        }

        private void btn_Input_Click(object sender, EventArgs e)
        {
            fromClipboard();
        }

        private void prepareText(string inputText)
        {
            text = inputText;
            if (cb_ReplaceWikiReference.Checked)
            {
                replaceWikiReference();
            }

            addSpaces('[');
            addSpaces(']', false);
            addSpaces('(');
            addSpaces(')', false);
            addSpaces('.', false);
            addSpaces('?', false);
            addSpaces('!', false);
            addSpaces('-', false);
            
            splitText = text.Split(' ').ToList<string>();
            wordCount = splitText.Count;
            label2.Text = "Words:" + wordCount.ToString();
            btnBack.Enabled = btnPause.Enabled = btnStart.Enabled = true;
            progressBar1.Maximum = wordCount;
            currentPos = 0;
            progressBar1.Value = currentPos;

            if(splitText.Count>0)
                SetText(splitText[0]);
        }

        private void addSpaces(char replace, bool before = true)
        {
           int startPos = 0;
           int pos = text.IndexOf(replace, startPos);
            while (pos > 0 && pos < text.Length)
            {
                if (before)
                {
                    if (text[pos - 1] != ' ')
                        text = text.Insert(pos, " ");
                }
                else
                {
                    if (pos + 1 == text.Length)
                    {
                        ++pos;
                        continue;
                    }

                    char c = text[pos + 1];
                    if (c != ' ')
                        text = text.Insert(pos+1, " ");
                }
                    pos = text.IndexOf(replace, pos+1);
            }
        }

        private void replaceWikiReference()
        {
            int startPos = 0;
            int pos = text.IndexOf('[',startPos);
            while (pos>0 && pos < text.Length)
            {
                int endPos = text.IndexOf(']', pos);
                if (endPos < 0)
                { ++pos;  continue; }

                string subtext = text.Substring(pos+1, endPos - pos-1);
                try
                {
                    Convert.ToInt32(subtext);
                    text = text.Substring(0, pos) + text.Substring(endPos + 1);
                    pos = text.IndexOf('[', endPos);
                }
                catch (FormatException ex)
                {
                    pos = text.IndexOf('[', endPos);
                }
                catch (ArgumentOutOfRangeException are)
                {

                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            c = config.loadConfig();
            nud_WPM.Value = (decimal)c.wpm;
            nudCommaPause.Value = (decimal)c.pauseComma;
            nudDotPause.Value = (decimal)c.pauseDot;
            cb_ReplaceWikiReference.Checked = c.replaceWikiRefs;
            tb_Word.MouseWheel += tb_Word_MouseWheel;
            tb_Word.MouseEnter += tb_Word_MouseEnter;
            cb_onTop.Checked = c.topMost;
            nud_Font.Value = (decimal)c.fontSize;
            tb_Word.Font = new Font(tb_Word.Font.FontFamily, (float)nud_Font.Value);
            this.TopMost = c.topMost;
        }

        void tb_Word_MouseEnter(object sender, EventArgs e)
        {
            tb_Word.Focus();
        }

        void tb_Word_MouseWheel(object sender, MouseEventArgs e)
        {
            shiftPos(e.Delta);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            c.wpm = (int)nud_WPM.Value;
            c.pauseComma = (int)nudCommaPause.Value;
            c.pauseDot = (int)nudDotPause.Value;
            c.replaceWikiRefs = cb_ReplaceWikiReference.Checked;
            c.topMost = cb_onTop.Checked;
            c.fontSize = (int)nud_Font.Value;

            config.saveConfig(c);
        }



        private void tb_Word_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 37) //left
            { shiftPos(1); e.Handled = true; }
            else if (e.KeyValue == 39) //right 
            { shiftPos(-1); e.Handled = true; }
            else if (e.KeyValue == 32) //space 
            {
                e.Handled = true;
                if (!threadRunning)
                    start();
                else
                    pause();
            }
            else if (e.KeyCode == Keys.V && e.Modifiers == Keys.Control)
            { e.Handled = true; fromClipboard(); }
        }

        private void fromClipboard()
        {
            if (Clipboard.ContainsText())
                prepareText(Clipboard.GetText());
            else
                MessageBox.Show("Error: No Text in Clipboard!");
        }

        private void shiftPos(int shift)
        {
            pause();

            if (splitText.Count == 0)
                return;

            currentPos -= shift;
            if (currentPos < 0)
                currentPos = 0;
            else if (currentPos >= wordCount)
                currentPos = wordCount - 1;

            remainingWords("Words:" + (wordCount - currentPos).ToString() + "/" + wordCount.ToString());
            SetText(splitText[currentPos]);
            setBar(currentPos);
        }

        private void cb_onTop_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = cb_onTop.Checked;
        }

        private void nud_Font_ValueChanged(object sender, EventArgs e)
        {
            tb_Word.Font = new Font(tb_Word.Font.FontFamily, (float)nud_Font.Value);

        }
    }
}
