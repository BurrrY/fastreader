﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace FastReader
{
    public class config
    {
        public int wpm;
        public int pauseDot;
        public int pauseComma;
        public int fontSize;
        public bool replaceWikiRefs;
        public bool topMost;
        public static string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        public static string fileName = "FastRead_conf.xml";

        public config()
        {
            wpm = 350;
            pauseComma = 100;
            pauseDot = 120;
            replaceWikiRefs = true;
            topMost = true;
            fontSize = 18;
        }

        public static config loadConfig() {
            config c = new config();

            if (File.Exists(Path.Combine(config.path, config.fileName)))
            {
                // Create an instance of the XmlSerializer specifying type and namespace.
                XmlSerializer serializer = new
                XmlSerializer(typeof(config));

                // A FileStream is needed to read the XML document.
                FileStream fs = new FileStream(Path.Combine(config.path, config.fileName), FileMode.Open);
                XmlReader reader = XmlReader.Create(fs);


                // Use the Deserialize method to restore the object's state.
                c = (config)serializer.Deserialize(reader);
                fs.Close();
            }

            return c;
        }

        internal static void saveConfig(config co)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(config));            
            // Create an XmlTextWriter using a FileStream.
            Stream fs = new FileStream(Path.Combine(config.path, config.fileName), FileMode.Create);
            XmlWriter writer =  new XmlTextWriter(fs, Encoding.Unicode);
            // Serialize using the XmlTextWriter.
            serializer.Serialize(writer, co);
            writer.Close();
        }
    }
}
